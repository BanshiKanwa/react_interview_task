import React from 'react';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import ImageListItemBar from '@material-ui/core/ImageListItemBar';

const Gallery = (props)=>{
    console.log("photos",props.photos);
    return <div>
        <ImageList cellHeight={375} cols={3}>
            {props.photos.map(img=>{
              return <ImageListItem key={img.id}>
                    <img src={img.urls.regular} alt={img.alt_description}/>
                    <ImageListItemBar title={img.description} subtitle={img.user.name}/>
                    </ImageListItem>
            })}
        </ImageList>
    </div>
}
export default Gallery;