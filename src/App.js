import React, { useEffect, useState } from 'react';
import Gallery from './components/Gallery';
import Input from './components/Input';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import axios from "axios";
import Pagination from '@material-ui/lab/Pagination';

const App = ()=>{
  const [photos,setPhotos] = useState([]);
  const [loading,setLoading] = useState(true);
  const [formData,setFormData] = useState('');
  const [page,setPage] = useState();


  const fetchData=async(input,page)=>{
    setLoading(true)
   const response= await axios.get(`https://api.unsplash.com/search/photos?page=${page}&query=${!input?'yacht':input}&client_id=CSJt2XFFang6rCyWFelQ7Ncid2sHqbGirCIUyXgnD1w`)
    const data = await response.data;
    setPhotos(data);
    setLoading(false)
  }

  const handleChange=(event)=>{
    setFormData(event.target.value);
  }
  const handleSubmit=(event)=>{
    event.preventDefault();
    fetchData(formData)
  }
  const pageChange = (event,value)=>{
     setPage(value);
     fetchData(formData,value)
  }
  useEffect(()=>{
    fetchData();
  },[])

  if(loading) 
  return (
  <div
  style={{
    height:'100vh',
    display:'flex',
    justifyContent:'center',
    alignItems:'center'
    }}
    >
    <CircularProgress size={130}/>
    </div>);

return (
<Container>
  <Input change={handleChange} submit={handleSubmit}/>
  <Gallery photos={photos.results}/>
  <div style={{display:'flex',justifyContent:'center',margin:'1rem 0'}}>
  <Pagination 
   count={10} 
   variant="outlined"
   color="secondary"
   size="large" 
   onChange={pageChange}
   page={page}/>
  </div>
</Container>
)
}
export default App;
